JAVA CI/CD template
================

The template focuses on creating a CI/CD pipeline for Gitlab.

We are utilizing maven and maven plugins alongside gitlab-ci to create our pipelines.

Stages
------
Master Branch

![](readme_assets/pipelines.png)

![](readme_assets/success_build.png)

Feature Branch

![](readme_assets/feauture_pipeline.png)

Information
-----------
1) At first we run a simple "mvn clean compile" for the build stage.

2) At the second stage we run "mvn org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report ". Jacoco is a plugin that is used to create unit test reports. It creates a static webpage at target/site/jacoco/* but also an xml with the results for third party tools. We need the xml for the next page. At this stage we also upload the artifact target/surefire-reports/TEST-*.xml which are the test results and are used by the gitlab to be presented in the merge request page.

3) At this stage we run two jobs. The code coverage job takes the jacoco xml created at the previous stage and transforms it to a cobertura format which is used by Gitlab to present the covered lines of the unit tests in the merge request page. We also run the code quality job. Gitlab uses codeclimate analysis tool for the analysis and by adding the .codeclimate.yml at the root of the repository we also enable the java sonar plugin.

4) At the deploy stage we push the artifact to the maven repository of Gitlab and using jib plugin we also create and push the conatiner image of our application to Gitlab's container registry. For tagging the images we use the feauture branch name. For the master we tag the image based on the pom's version attribute.

Code Quality and Tests
----------------------

![](readme_assets/code_quality.png)

Code Coverage
-------------

![](readme_assets/test_coverage.png)


Artifacts
---------
![](readme_assets/artifacts.png)

Images
------
![](readme_assets/images.png)

![](readme_assets/image_tags.png)

