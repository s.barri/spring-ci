package com.magloo.springci;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@Slf4j
public class HelloController {

    @GetMapping("/hello")
    public String getHello() {
        return "Hello";
    }


    @GetMapping("/bye")
    public String goodName(boolean test) {
        String unused3;

        if (test) {
            log.info("test");
        }
        unused3 = "adssa";
        return unused3;
    }


    @GetMapping("/test")
    public String testName(boolean test) {
        String unused3;

        if (test) {
            log.info("test");
        }
        unused3 = "adssa";
        return unused3;
    }

}
